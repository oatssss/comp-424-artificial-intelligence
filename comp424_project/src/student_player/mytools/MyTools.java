package student_player.mytools;

import java.lang.invoke.ConstantCallSite;
import java.util.ArrayList;

import boardgame.Move;
import boardgame.Player;
import hus.HusBoardState;
import hus.HusMove;
import hus.HusPlayer;

public class MyTools {
    
    // Weights for the evaluation function
    private static final float WEIGHT_SEED_COUNT = 1f;
    private static final float WEIGHT_CAPTURABLE = 1f;
    private static final float WEIGHT_VULNERABLE = -0.5f;
    private static final float WEIGHT_SINGLES    = -0.25f;
    
    // Index for the first inner pit
    private static final int   INDEX_INNER_START = 16;
    
    // The evaluation function
    public static float evaluateBoardState(HusBoardState boardState, int playerID) {
        
    	int[][] pits    = boardState.getPits();
    	int[] 	my_pits = pits[playerID];
    	int[] 	op_pits = pits[(playerID + 1) % 2];
    	
    	// These are the metrics we'll use to evaluate a board
    	int totalOwnedSeeds       = 0;
    	int vulnerableSeeds       = 0;
    	int capturableSeeds       = 0;
    	int singleFilledOwnedPits = 0;
    	
    	// Check each owned pit for #seeds, #single filled pits, #vulnerable seeds
    	for (int pitIndex = 0; pitIndex < my_pits.length; pitIndex++) {
    	     
    	    int seedsInPit = my_pits[pitIndex];
    	    
    	    // Update #Seeds
            totalOwnedSeeds += seedsInPit;
            
            // Update #Single filled pits
            if (seedsInPit == 1)
                { singleFilledOwnedPits++; }
            
            // Update #Vulnerable and #Capturable seeds
            if (isInnerPit(pitIndex)) {
                vulnerableSeeds += getVulnerableSeeds(pitIndex, my_pits, op_pits);
                capturableSeeds += getCapturableSeeds(pitIndex, my_pits, op_pits);
            }
        }
    	
		return totalOwnedSeeds*WEIGHT_SEED_COUNT + capturableSeeds*WEIGHT_CAPTURABLE + vulnerableSeeds*WEIGHT_VULNERABLE + singleFilledOwnedPits*WEIGHT_SINGLES;
    }
    
    private static boolean isInnerPit(int pitIndex)
    {
    	return pitIndex >= INDEX_INNER_START;
    }
    
    private static int getCapturableSeeds(int innerPitIndex, int[] playerPits, int[] opponentPits)
    {
    	int innerPitPlayerSeeds = playerPits[innerPitIndex];
    	// We can only capture if the end-pit is already occupied
    	if (innerPitPlayerSeeds <= 0)
    	    { return 0; }
    	
    	int innerPitOpponentIndex = getCorrespondingInnerIndex(innerPitIndex);
    	int innerPitOpponentSeeds = opponentPits[innerPitOpponentIndex];
    	// We can only capture if the pit across the end-pit has seeds in it
    	if (innerPitOpponentSeeds <= 0)
    	    { return 0; }
    	
    	int outerPitOpponentSeeds = opponentPits[getCorrespondingOuterIndex(innerPitOpponentIndex)];
    	
    	return innerPitOpponentSeeds + outerPitOpponentSeeds;
    }
    
    private static int getVulnerableSeeds(int innerPitIndex, int[] playerPits, int[] opponentPits)
    {
    	int innerPitPlayerSeeds = playerPits[innerPitIndex];
    	// The opponent can only steal if there's seeds in the inner pit
    	if (innerPitPlayerSeeds <= 0)
    	    { return 0; }
    	
    	int innerPitOpponentSeeds = opponentPits[getCorrespondingInnerIndex(innerPitIndex)];
    	// The opponent can only steal if the end-pit is already occupied
    	if (innerPitOpponentSeeds <= 0)
    	    { return 0; }
    	
    	int outerPitPlayerSeeds = playerPits[getCorrespondingOuterIndex(innerPitIndex)];
    	
    	return innerPitPlayerSeeds + outerPitPlayerSeeds;
    }
    
    /**
     * Returns the index of the pit lying on the outside of innerPitIndex.
     * @param innerPitIndex The index of the inner pit.
     * @return The index of the associated outer pit.
     */
    private static int getCorrespondingOuterIndex(int innerPitIndex)
    {
        return 31 - innerPitIndex;
    }
    
    /**
     * Returns the index of the pit lying across from innerPitIndex in the opponent's side.
     * @param innerPitIndex The index of the inner pit.
     * @return The index of the associated inner opponent pit.
     */
    private static int getCorrespondingInnerIndex(int innerPitIndex)
    {
        return getCorrespondingOuterIndex(innerPitIndex) + 16;
    }
    
    public static HusMove minimax(HusBoardState boardState, int maxDepth)
    {
        int playerID = boardState.getTurnPlayer();
        ArrayList<HusMove> moves = boardState.getLegalMoves();
        
        HusMove bestMove = null;
        float bestSoFar = Float.NEGATIVE_INFINITY;
        float alpha = Float.NEGATIVE_INFINITY;
        float beta = Float.POSITIVE_INFINITY;
        for (HusMove move : moves) {
            HusBoardState cloned_board_state = (HusBoardState) boardState.clone();
            cloned_board_state.move(move);
            float evaluation = prunedTraverse(cloned_board_state, alpha, beta, 1, maxDepth, false, playerID);
            if (evaluation > bestSoFar) {
                bestMove = move;
                bestSoFar = evaluation;
            }
            alpha = Math.max(alpha, bestSoFar);
            if (beta <= alpha)
                { break; }
        }
        
        return bestMove;
    }
//    */
    
    /*
     * The following two methods were used in the naive minimax implementation
     * 
    public static float minPlay(HusBoardState boardState, int playerID, int currentDepth, int maxDepth)
    {
        if (boardState.gameOver() || currentDepth >= maxDepth)
            { return evaluateBoardState(boardState, playerID); }
        
        ArrayList<HusMove> moves = boardState.getLegalMoves();
        float worstEvaluation = Float.POSITIVE_INFINITY;
        for (HusMove move : moves) {
            HusBoardState cloned_board_state = (HusBoardState) boardState.clone();
            cloned_board_state.move(move);
            float evaluation = maxPlay(cloned_board_state, playerID, currentDepth+1, maxDepth);
            if (evaluation < worstEvaluation)
                { worstEvaluation = evaluation; }
        }
        
        return worstEvaluation;
        
    }
    
    public static float maxPlay(HusBoardState boardState, int playerID, int currentDepth, int maxDepth)
    {
        if (boardState.gameOver() || currentDepth >= maxDepth)
            { return evaluateBoardState(boardState, playerID); }
        
        ArrayList<HusMove> moves = boardState.getLegalMoves();
        float bestEvaluation = Float.NEGATIVE_INFINITY;
        for (HusMove move : moves) {
            HusBoardState cloned_board_state = (HusBoardState) boardState.clone();
            cloned_board_state.move(move);
            float evaluation = minPlay(cloned_board_state, playerID, currentDepth+1, maxDepth);
            if (evaluation > bestEvaluation)
                { bestEvaluation = evaluation; }
        }
        
        return bestEvaluation;
    }
    */
    
    public static float prunedTraverse(HusBoardState boardState, float alpha, float beta, int currentDepth, int maxDepth, boolean maximizing, int playerID)
    {        
        if (boardState.gameOver() || currentDepth > maxDepth)
            { return evaluateBoardState(boardState, playerID); }
        
        ArrayList<HusMove> moves = boardState.getLegalMoves();
        
        if (maximizing) {
            float bestSoFar = Float.NEGATIVE_INFINITY;
            for (HusMove move : moves) {
                HusBoardState cloned_board_state = (HusBoardState) boardState.clone();
                cloned_board_state.move(move);
                float evaluation = prunedTraverse(cloned_board_state, alpha, beta, currentDepth+1, maxDepth, false, playerID);
                bestSoFar = Math.max(evaluation, bestSoFar);
                alpha = Math.max(alpha, bestSoFar);
                if (beta <= alpha)
                    { break; }
            }
            return bestSoFar;
        }
        
        else {
            float worstSoFar = Float.POSITIVE_INFINITY;
            for (HusMove move : moves) {
                HusBoardState cloned_board_state = (HusBoardState) boardState.clone();
                cloned_board_state.move(move);
                float evaluation = prunedTraverse(cloned_board_state, alpha, beta, currentDepth+1, maxDepth, true, playerID);
                worstSoFar = Math.min(evaluation, worstSoFar);
                beta = Math.min(beta, worstSoFar);
                if (beta <= alpha)
                    { break; }
            }
            return worstSoFar;
        }
        
        /*  Original pseudocode from wikipedia:
         * 
         *  01 function alphabeta(node, depth, α, β, maximizingPlayer)
            02      if depth = 0 or node is a terminal node
            03          return the heuristic value of node
            04      if maximizingPlayer
            05          v := -∞
            06          for each child of node
            07              v := max(v, alphabeta(child, depth - 1, α, β, FALSE))
            08              α := max(α, v)
            09              if β ≤ α
            10                  break (* β cut-off *)
            11          return v
            12      else
            13          v := ∞
            14          for each child of node
            15              v := min(v, alphabeta(child, depth - 1, α, β, TRUE))
            16              β := min(β, v)
            17              if β ≤ α
            18                  break (* α cut-off *)
            19          return v
         */
    }
}
